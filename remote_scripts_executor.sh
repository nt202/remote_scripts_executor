#!/bin/sh

if [ $# -eq 0 ]
    then
        echo "No arguments provided"


elif [ $1 = "install" ]
    then
        echo "Installing ..."

        mv ./remote_scripts_executor /usr/bin/remote_scripts_executor
        chmod +x /usr/bin/remote_scripts_executor

        echo "" > /etc/systemd/system/remote_scripts_executor.service

        echo "[Unit]"                                     >> /etc/systemd/system/remote_scripts_executor.service
        echo "Description=remote_scripts_executor"        >> /etc/systemd/system/remote_scripts_executor.service
        echo "After=syslog.target"                        >> /etc/systemd/system/remote_scripts_executor.service
        echo "[Service]"                                  >> /etc/systemd/system/remote_scripts_executor.service
        echo "ExecStart=/usr/bin/remote_scripts_executor" >> /etc/systemd/system/remote_scripts_executor.service
        echo "Restart=always"                             >> /etc/systemd/system/remote_scripts_executor.service
        echo "KillSignal=SIGKILL"                         >> /etc/systemd/system/remote_scripts_executor.service
        echo "Type=simple"                                >> /etc/systemd/system/remote_scripts_executor.service
        echo "StandardError=syslog"                       >> /etc/systemd/system/remote_scripts_executor.service
        echo "NotifyAccess=all"                           >> /etc/systemd/system/remote_scripts_executor.service
        echo "SyslogIdentifier=remote_scripts_executor"   >> /etc/systemd/system/remote_scripts_executor.service
        echo "[Install]"                                  >> /etc/systemd/system/remote_scripts_executor.service
        echo "WantedBy=default.target"                    >> /etc/systemd/system/remote_scripts_executor.service

        mkdir -p /etc/remote_scripts_executor

        echo '' > /etc/remote_scripts_executor/remote_scripts_executor.json

        echo '{'                     >> /etc/remote_scripts_executor/remote_scripts_executor.json
        echo '  "port": 10555,'      >> /etc/remote_scripts_executor/remote_scripts_executor.json
        echo '  "remote": "gitlab",' >> /etc/remote_scripts_executor/remote_scripts_executor.json
        echo '  "no_proxy": false,'  >> /etc/remote_scripts_executor/remote_scripts_executor.json
        echo '  "period_sec": 30,'   >> /etc/remote_scripts_executor/remote_scripts_executor.json
        echo '  "scripts": ['        >> /etc/remote_scripts_executor/remote_scripts_executor.json
        echo '  ]'                   >> /etc/remote_scripts_executor/remote_scripts_executor.json
        echo '}'                     >> /etc/remote_scripts_executor/remote_scripts_executor.json
        echo ''                      >> /etc/remote_scripts_executor/remote_scripts_executor.json

        mkdir -p /var/log/remote_scripts_executor

        systemctl daemon-reload
        systemctl start remote_scripts_executor.service
        systemctl enable remote_scripts_executor.service
        systemctl daemon-reload

        echo "Done!"


elif [ $1 = "uninstall" ]
    then
        echo "Uninstalling ..."

        systemctl daemon-reload
        systemctl stop remote_scripts_executor.service
        systemctl disable remote_scripts_executor.service
        rm -rf /etc/systemd/system/remote_scripts_executor.service
        systemctl daemon-reload
        
        rm -rf /usr/bin/remote_scripts_executor
        rm -rf /etc/remote_scripts_executor
        rm -rf /var/run/remote_scripts_executor.pid
        rm -rf /var/log/remote_scripts_executor

        echo "Done!"


else
    echo "Invalid argument"
fi
