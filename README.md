# remote_scripts_executor
https://gitlab.com/nt202/remote_scripts_executor    
Listens to changes on script files on remote (for now only gitlab). On any change - executes them.    
It has GUI as well, available at http://0.0.0.0:10555/    
    
![screenshot.png](screenshot.png)

    
#### Install on x86_64:
```bash
curl https://gitlab.com/nt202/remote_scripts_executor/-/package_files/172547019/download -o remote_scripts_executor.sh
curl https://gitlab.com/nt202/remote_scripts_executor/-/package_files/172547385/download -o remote_scripts_executor
sudo chmod +x remote_scripts_executor.sh
sudo ./remote_scripts_executor.sh install
```

    
#### Install on Raspberry Pi:
```bash
curl https://gitlab.com/nt202/remote_scripts_executor/-/package_files/172547019/download -o remote_scripts_executor.sh
curl https://gitlab.com/nt202/remote_scripts_executor/-/package_files/172547967/download -o remote_scripts_executor
sudo chmod +x remote_scripts_executor.sh
sudo ./remote_scripts_executor.sh install
```

    
#### Uninstall:
```bash
curl https://gitlab.com/nt202/remote_scripts_executor/-/package_files/172547019/download -o remote_scripts_executor.sh
sudo chmod +x remote_scripts_executor.sh
sudo ./remote_scripts_executor.sh uninstall
```

    
#### Examples of /etc/remote_scripts_executor/remote_scripts_executor.json:
```json
{
  "port": 10555,
  "remote": "gitlab",
  "no_proxy": true,
  "period_sec": 30,
  "scripts": [
    {
      "skip": false,
      "script_file": "/etc/remote_scripts_executor/remote_scripts_executor_gitlab_ci.sh",
      "access_token": "glpat-KJLQsLoD_i2z_ytE1sX_",
      "script_url": "https://gitlab.com/api/v4/projects/46568812/repository/files/.gitlab-ci.sh?ref=main"
    }
  ]
}
```

    
#### You also can define you environment variables in /etc/systemd/system/remote_scripts_executor.service:
```conf
[Service]
Environment="GITLAB_PERSONAL_ACCESS_TOKEN=glpat-ahdHyhjkJkQhBS7AZuhe"
Environment="SECRET_PASSWORD=O5eBBz1234"
```

And then you can use it in your scripts or in your configuration:
```json
{
  "port": 10555,
  "remote": "gitlab",
  "no_proxy": true,
  "period_sec": 30,
  "scripts": [
    {
      "skip": false,
      "script_file": "/etc/remote_scripts_executor/remote_scripts_executor_gitlab_ci.sh",
      "access_token": "$GITLAB_PERSONAL_ACCESS_TOKEN",
      "script_url": "https://gitlab.com/api/v4/projects/46568812/repository/files/.gitlab-ci.sh?ref=main"
    }
  ]
}
```