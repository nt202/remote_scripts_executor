#!/bin/bash

# chmod +x ex.sh

if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <argument>" >&2;
    exit 1;
fi

if [ "$1" == "run" ]; then
    RUST_BACKTRACE=1 cargo run;
	exit 0;
fi

if [ "$1" == "build" ]; then
    cargo build;
	exit 0;
fi

if [ "$1" == "format" ]; then
    cargo fmt;
	exit 0;
fi

echo "Unknown argument: $1" >&2;
exit 1;
