# cargo install cross --git https://github.com/cross-rs/cross

# Preparing:
PATH=/root/.cargo/bin:$PATH
ls -l /proc/$$/exe
echo $GITLAB_PERSONAL_ACCESS_TOKEN

# Variables:
REPO_NAME=remote_scripts_executor
REPO_ID=46568812

# Cloning:
mkdir -p /data
(cd /data && rm -rf $REPO_NAME)
(cd /data && git clone https://gitlab.com/nt202/remote_scripts_executor.git $REPO_NAME)

# Get version:
CURRENT_VERSION=$(cd /data/$REPO_NAME && cargo metadata --no-deps --format-version 1 | jq -r '.packages[0].version')

# Install-Uninstall script:
(cd /data/$REPO_NAME && curl -X PUT -H "PRIVATE-TOKEN: ${GITLAB_PERSONAL_ACCESS_TOKEN}" --upload-file remote_scripts_executor.sh "https://gitlab.com/api/v4/projects/46568812/packages/generic/${CURRENT_VERSION}/build/remote_scripts_executor.sh")

# x86_64:
(cd /data/$REPO_NAME && cross build --release --target=x86_64-unknown-linux-musl)
(cd /data/$REPO_NAME && mv target/x86_64-unknown-linux-musl/release/remote_scripts_executor ./remote_scripts_executor_x86_64)
(cd /data/$REPO_NAME && curl -X PUT -H "PRIVATE-TOKEN: ${GITLAB_PERSONAL_ACCESS_TOKEN}" --upload-file remote_scripts_executor_x86_64 "https://gitlab.com/api/v4/projects/46568812/packages/generic/${CURRENT_VERSION}/build/remote_scripts_executor_x86_64")

# Raspberry:
(cd /data/$REPO_NAME && cross build --release --target=armv7-unknown-linux-musleabihf)
(cd /data/$REPO_NAME && mv target/armv7-unknown-linux-musleabihf/release/remote_scripts_executor ./remote_scripts_executor_rapsberry)
(cd /data/$REPO_NAME && curl -X PUT -H "PRIVATE-TOKEN: ${GITLAB_PERSONAL_ACCESS_TOKEN}" --upload-file remote_scripts_executor_rapsberry "https://gitlab.com/api/v4/projects/46568812/packages/generic/${CURRENT_VERSION}/build/remote_scripts_executor_raspberry")
