use std::collections::BTreeMap;

use super::websocket_protocol::WrapperWebsocketMessage;
use actix::Addr;
use actix_web::{web, HttpRequest, HttpResponse, Responder};
use actix_web_actors::ws;
use log::error;
use serde::Deserialize;
use std::time::Instant;
use tokio::sync::mpsc::channel;
use uuid::Uuid;

use crate::{
    app_state::AppState,
    time_now,
    websocket::{websocket_connection::SendToUnauthorized, websocket_session::WebsocketSession},
};

use super::websocket_connection::WebsocketConnections;

pub async fn ws(request: HttpRequest, stream: web::Payload, data: web::Data<AppState>) -> impl Responder {
    let guid = Uuid::new_v4().to_string();
    println!("{} | {} | New websocket connection attempt {}", time_now(), "ugAIye", guid);
    return ws::start(
        WebsocketSession {
            guid: guid,
            hb: Instant::now(),
            addr_ws_connections_storage: data.get_ref().addr_ws_connections_storage.clone(),
        },
        &request,
        stream,
    );
}
