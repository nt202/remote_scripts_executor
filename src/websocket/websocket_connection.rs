use crate::websocket::websocket_session::WebsocketResponse;
use actix::prelude::*;
use actix::{Actor, Context, Handler, Recipient};
use r2d2::Pool;
use std::collections::HashMap;
use std::sync::Arc;

pub const WS_USERNAME_GUID_DELIMTER: &str = "|";

fn to_key(username: String, guid: String) -> String {
    format!("{}{}{}", username, WS_USERNAME_GUID_DELIMTER, guid)
}

fn from_key(key: &String) -> String {
    key.split(WS_USERNAME_GUID_DELIMTER).next().expect("miHhJx").to_string()
}

#[derive(Debug)]
pub struct WebsocketConnections {
    pub authorized: HashMap<String, Recipient<WebsocketResponse>>,
    pub unauthorized: HashMap<String, Recipient<WebsocketResponse>>,
}

impl WebsocketConnections {
    pub fn new() -> WebsocketConnections {
        WebsocketConnections {
            authorized: HashMap::new(),
            unauthorized: HashMap::new(),
        }
    }
}

impl Actor for WebsocketConnections {
    type Context = Context<Self>;
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct Add {
    pub guid: String,
    pub addr: Recipient<WebsocketResponse>,
}

impl Handler<Add> for WebsocketConnections {
    type Result = ();

    #[rustfmt::skip]
    fn handle(&mut self, msg: Add, _: &mut Self::Context) {
        self.unauthorized.insert(msg.guid, msg.addr);
    }
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct Delete {
    pub guid: String,
}

impl Handler<Delete> for WebsocketConnections {
    type Result = ();

    fn handle(&mut self, msg: Delete, _: &mut Self::Context) {
        self.unauthorized.remove(&msg.guid);
    }
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct Online {}

impl Handler<Online> for WebsocketConnections {
    type Result = ();

    fn handle(&mut self, _: Online, _: &mut Self::Context) {
        for ele in &self.unauthorized {
            let addr = ele.1;
            addr.do_send(WebsocketResponse { body: "joiojoijio".to_string() });
        }
    }
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct SendRaw {
    pub text: String,
}

impl Handler<SendRaw> for WebsocketConnections {
    type Result = ();
    fn handle(&mut self, onn: SendRaw, _: &mut Self::Context) {
        for ele in &self.unauthorized {
            let addr = ele.1;
            addr.do_send(WebsocketResponse { body: onn.text.clone() });
        }
    }
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct SendToUnauthorized {
    pub guids: Vec<String>,
    pub text: String,
}

impl Handler<SendToUnauthorized> for WebsocketConnections {
    type Result = ();
    fn handle(&mut self, msg: SendToUnauthorized, _: &mut Self::Context) {
        for guid in msg.guids {
            if let Some(addr) = self.unauthorized.get(&guid) {
                addr.do_send(WebsocketResponse { body: msg.text.clone() });
            } else {
                println!("ceqMye: User {} is not unauthorized", guid);
            }
        }
    }
}
