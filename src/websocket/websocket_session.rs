use std::time::{Duration, Instant};

use super::websocket_connection::{SendRaw, SendToUnauthorized};
use crate::time_now;
use crate::websocket::websocket_connection::{Add, Delete, WebsocketConnections};
use actix::prelude::fut;
use actix::prelude::*;
use actix::{Actor, ActorContext, Addr, AsyncContext, Handler, Running, StreamHandler};
use actix_web_actors::ws;
use actix_web_actors::ws::{Message, ProtocolError};
use chrono::Utc;
use log::{debug, info, warn};

const HEARTBEAT_INTERVAL: Duration = Duration::from_secs(10);
const CLIENT_TIMEOUT: Duration = Duration::from_secs(20);

pub struct WebsocketSession {
    pub guid: String,
    pub hb: Instant,
    pub addr_ws_connections_storage: Addr<WebsocketConnections>,
}

impl Actor for WebsocketSession {
    type Context = ws::WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.hb(ctx);
        let addr = ctx.address();
        let guid = &self.guid;
        self.addr_ws_connections_storage
            .send(Add {
                guid: guid.clone(),
                addr: addr.recipient(),
            })
            .into_actor(self)
            .then(|_res, _act, _ctx| fut::ready(()))
            .wait(ctx);
        self.addr_ws_connections_storage.do_send(SendToUnauthorized {
            guids: vec![guid.clone()],
            text: format!("{} | {} | Connected to websocket, your guid = {}", time_now(), "nzhV1F", guid),
        });
        println!("{} | {} | User {} connected", time_now(), "0UzIAx", self.guid);
    }

    fn stopping(&mut self, _: &mut Self::Context) -> Running {
        self.addr_ws_connections_storage.do_send(Delete { guid: self.guid.clone() });
        println!("{} | {} | User {} disconnected", time_now(), "qHXarK", self.guid);
        Running::Stop
    }
}

impl WebsocketSession {
    fn hb(&self, ctx: &mut ws::WebsocketContext<Self>) {
        ctx.run_interval(HEARTBEAT_INTERVAL, |act, ctx| {
            if Instant::now().duration_since(act.hb) > CLIENT_TIMEOUT {
                println!("Disconnecting, heartbeat failed");
                act.addr_ws_connections_storage.do_send(Delete { guid: act.guid.clone() });
                ctx.stop();
                return;
            }
            ctx.ping(Utc::now().timestamp_millis().to_string().as_bytes());
        });
    }
}

#[rustfmt::skip]
impl StreamHandler<Result<ws::Message, ProtocolError>> for WebsocketSession {
    fn handle(&mut self, msg: Result<Message, ProtocolError>, ctx: &mut Self::Context) {
        match msg {
            Ok(ws::Message::Ping(bin)) => {
                self.hb = Instant::now();
                ctx.pong(&bin);
            }
            Ok(ws::Message::Pong(_)) => {
                self.hb = Instant::now();
            }
            Ok(ws::Message::Text(text)) => {
                // info!(marker = "OsEsjK"; "{}", &text);
                // let jrpc: JrpcRequest = serde_json::from_str(&text).unwrap();
                // let lang = jrpc.lang;
                // // let option_cache_key = jrpc.cache_key.clone();
                // if self.option_sub.is_none() {
                //     if jrpc.method == Method::ValidateUsername {
                //         ctx.text(validation_service::validate_username(jrpc, lang));
                //         return;
                //     }
                //     if jrpc.method == Method::ValidatePassword {
                //         ctx.text(validation_service::validate_password(jrpc, lang));
                //         return;
                //     }
                //     warn!(marker = "fyYail"; "{}", "User is none");
                //     ctx.text(r#"<div id="id_pong">PONG</div>"#);
                //     return;
                // }
                // let sub = self.option_sub.as_ref().unwrap().clone();
                // // info!(marker = "OsEsjK", sub = sub.as_str(); "{}", &text);
                // match jrpc.method {
                //     Method::ChatsGet => {
                //         self.addr_chat_service.do_send(ChatsRequest { jrpc, sub, option_web_cache: None });
                //     },
                //     Method::V1UserGet => todo!(),
                //     Method::ValidateUsername => {
                //         ctx.text(validation_service::validate_username(jrpc, lang));
                //     },
                //     Method::ValidatePassword => {
                //         ctx.text(validation_service::validate_password(jrpc, lang));
                //     },
                //     Method::V1Search => todo!(),
                // }
                // ctx.text(text);
            }
            Ok(ws::Message::Binary(_bin)) => {}
            Ok(ws::Message::Close(reason)) => {
                ctx.close(reason);
                ctx.stop();
            }
            _ => ctx.stop(),
        }
    }
}

#[derive(Clone, Message)]
#[rtype(result = "()")]
pub struct WebsocketResponse {
    pub body: String,
}

impl Handler<WebsocketResponse> for WebsocketSession {
    type Result = ();

    fn handle(&mut self, msg: WebsocketResponse, ctx: &mut Self::Context) -> Self::Result {
        ctx.text(msg.body);
    }
}
