use crate::websocket::generic_error::GenericError;
use serde::{Deserialize, Deserializer, Serialize};
use strum::IntoEnumIterator;
use strum_macros::EnumIter;

#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash, EnumIter)]
pub enum Lang {
    DE,
    EN,
    ES,
    FR,
    RU,
}

impl Lang {
    pub fn parse(code: String) -> Self {
        if code.starts_with("fr") || code == "FR" {
            return Lang::FR;
        }
        if code.starts_with("de") || code == "DE" {
            return Lang::DE;
        }
        if code.starts_with("ru") || code == "RU" {
            return Lang::RU;
        }
        if code.starts_with("es") || code == "ES" {
            return Lang::ES;
        }
        Lang::EN
    }

    pub fn to_flag(&self) -> &'static str {
        match &self {
            Lang::DE => "🇩🇪",
            Lang::EN => "🇬🇧",
            Lang::ES => "🇪🇸",
            Lang::FR => "🇫🇷",
            Lang::RU => "🇷🇺",
        }
    }

    pub fn to_native(&self) -> &'static str {
        match &self {
            Lang::DE => "Deutsch",
            Lang::EN => "English",
            Lang::ES => "Español",
            Lang::FR => "Français",
            Lang::RU => "Русский",
        }
    }

    // https://en.wikipedia.org/wiki/List_of_ISO_639_language_codes
    pub fn to_iso639(&self) -> &'static str {
        match &self {
            Lang::DE => "de",
            Lang::EN => "en",
            Lang::ES => "es",
            Lang::FR => "fr",
            Lang::RU => "ru",
        }
    }

    pub fn to_string(&self) -> String {
        format!("{:?}", self)
    }
}

impl<'de> Deserialize<'de> for Lang {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s: &str = Deserialize::deserialize(deserializer)?;
        for lang in Lang::iter() {
            let str = format!("{:?}", lang);
            if s == str {
                return Ok(lang);
            }
        }
        Err(serde::de::Error::custom(GenericError::str("5EAZze", "Unexpected lang")))
    }
}

impl Serialize for Lang {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let str = format!("{:?}", self);
        serializer.serialize_str(&str)
    }
}

#[cfg(test)]
mod tests {
    use crate::lang::Lang;

    #[test]
    fn to_flag() {
        let lang: Lang = Lang::FR;
        let flag = lang.to_flag();
        println!("{}", flag);
    }
}
