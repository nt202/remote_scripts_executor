use std::fmt::Display;

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug)]
pub struct GenericError {
    pub code: i32,
    pub marker: String,
    pub message: String,
}

impl GenericError {
    pub fn new(marker: &'static str, message: String) -> Self {
        GenericError {
            code: -32000,
            marker: marker.to_string(),
            message,
        }
    }
    pub fn str(marker: &'static str, message: &'static str) -> String {
        GenericError {
            code: -32000,
            marker: marker.to_string(),
            message: message.to_string(),
        }
        .to_string()
    }
    pub fn string(marker: &'static str, message: String) -> String {
        GenericError {
            code: -32000,
            marker: marker.to_string(),
            message,
        }
        .to_string()
    }
}

impl Display for GenericError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", serde_json::to_string(self).unwrap())
    }
}

#[cfg(test)]
mod tests {
    use crate::generic_error::GenericError;

    #[test]
    fn serialize_test() {
        println!(
            "{}",
            GenericError {
                code: -32000,
                marker: "omqRRc".to_string(),
                message: "error text example".to_string()
            }
        );
    }
}
