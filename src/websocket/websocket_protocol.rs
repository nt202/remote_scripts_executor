use serde::{Deserialize, Deserializer, Serialize};
use strum::IntoEnumIterator;
use strum_macros::EnumIter;

use crate::websocket::{generic_error::GenericError, lang::Lang};

#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash, EnumIter)]
pub enum Method {
    ChatsGet,
    ChatOpen,
    ChatGet,
    ChatSend,
    ChatSeen,
    Content,
    V1ProfileGet,
    V1UserGet,
    ProfileSaveAvatar,
    ProfileSaveFirstName,
    ProfileSaveLastName,
    ProfileSaveCatchphrase,
    ProfileSaveCountry,
    ProfileSaveAboutYourself,
    TimekillingGet,
    UserGet,
    UsersGet,
    ValidateUsername,
    ValidatePassword,
    V1Search,
}

impl Serialize for Method {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let str = format!("{:?}", self);
        serializer.serialize_str(&str)
    }
}

impl<'de> Deserialize<'de> for Method {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s: &str = Deserialize::deserialize(deserializer)?;
        for method in Method::iter() {
            let str = format!("{:?}", method);
            if s == str {
                return Ok(method);
            }
        }
        Err(serde::de::Error::custom(GenericError::str("mGGIhf", "Unable to parse method")))
    }
}

pub struct WrapperWebsocketMessage {
    pub keys: Vec<String>, // usernames or guids.
    pub json: String,      // body, actually is json of Jrpc, for fast transmitting, serialization in services.
}

#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash, EnumIter)]
pub enum ResponseType {
    JSON,
    HTML,
}

impl Serialize for ResponseType {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let str = format!("{:?}", self);
        serializer.serialize_str(&str)
    }
}

impl<'de> Deserialize<'de> for ResponseType {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s: &str = Deserialize::deserialize(deserializer)?;
        for response_type in ResponseType::iter() {
            let str = format!("{:?}", response_type);
            if s == str {
                return Ok(response_type);
            }
        }
        Err(serde::de::Error::custom(GenericError::str("PCFabZ", "Unexpected response type")))
    }
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug)]
pub struct JrpcResponse {
    pub jsonrpc: &'static str,
    pub id: String,
    pub method: Method,
    pub request_time: u128,  // unix ms
    pub response_time: u128, // unix ms
    pub response_type: ResponseType,
    pub lang: Lang,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub error: Option<GenericError>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub result: Option<serde_json::Value>,
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug)]
pub struct JrpcRequest {
    pub jsonrpc: String,
    pub id: String,
    pub method: Method,
    pub request_time: u128, // unix ms
    pub response_type: ResponseType,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub android_id: Option<String>,
    pub lang: Lang,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub params: Option<serde_json::Value>,
}

// impl Jrpc {
//     pub fn get_lang(&self) -> Lang {
//         Lang::parse(self.lang_cookie.as_ref().expect("n1t7Qd").to_string())
//     }

//     // pub fn get_search_value(&self, argument: &'static str) -> Option<String> {
//     //     // Split the search string into key-value pairs
//     //     let params: Vec<&str> = self.search.trim_start_matches('?').split('&').collect();

//     //     // Iterate over the key-value pairs
//     //     for param in params {
//     //         // Split each key-value pair into key and value
//     //         let pair: Vec<&str> = param.split('=').collect();
//     //         if pair.len() == 2 && pair[0] == argument {
//     //             // If the key matches the argument, return the value
//     //             return Some(pair[1].to_string());
//     //         }
//     //     }
//     //     // If the argument is not found, return None
//     //     None
//     // }
// }

#[cfg(test)]
mod tests {
    use crate::websocket_protocol::JrpcRequest;

    #[test]
    fn query_test() {
        let json = r###"
        {
            "jsonrpc": "2.0",
            "pathname": "/users",
            "search": "?page=1&size=20&orderBy=name",
            "method": "V1ProfileGet",
            "lang": "EN",
            "id": "UWYbA74tym5L",
            "request_time": 1693487842475,
            "response_type": "JSON",
            "params": {"k": "v"}
        }
        "###;
        let request: JrpcRequest = serde_json::from_str(json).unwrap();
        // let page_value = request.get_search_value("orderBy");
        println!("{:?}", request); // Output: Some("1")
    }

    #[test]
    fn deserialize_test() {
        let json = r###"
        {
            "jsonrpc": "2.0",
            "pathname": "/users",
            "search": "?page=1",
            "method": "V1ProfileGet",
            "id": "UWYbA74tym5L",
            "lang": "RU",
            "request_time": 1693487842475,
            "response_type": "JSON",
            "params": {"k": "v"}
        }
        "###;
        let request: JrpcRequest = serde_json::from_str(json).unwrap();
        println!("{:?}", request);
    }

    #[test]
    fn serialize_test() {
        let json1 = r###"
        {
            "jsonrpc": "2.0",
            "pathname": "/users",
            "search": "?page=1",
            "method": "V1ProfileGet",
            "id": "UWYbA74tym5L",
            "lang": "ES",
            "request_time": 1693487842475,
            "response_type": "JSON",
            "params": {"k":"v"}
        }
        "###;
        let response1: JrpcRequest = serde_json::from_str(json1).unwrap();
        println!("{}", serde_json::to_string(&response1).unwrap());

        let json2 = r###"
        {
            "jsonrpc": "2.0",
            "pathname": "/users",
            "search": "?page=1",
            "method": "V1ProfileGet",
            "id": "UWYbA74tym5L",
            "lang": "RU",
            "request_time": 1693487842475,
            "response_type": "JSON",
            "error": {"code": -32000, "marker": "gG6kU2", "message": "Something goes wrong"}
        }
        "###;
        let response2: JrpcRequest = serde_json::from_str(json2).unwrap();
        println!("{}", serde_json::to_string(&response2).unwrap());
    }
}
