use byte_unit::Byte;
use chrono::{
    format::{DelayedFormat, Fixed, Item},
    DateTime, Local,
};
use log::LevelFilter;
use log::{Level, Record};
use log4rs::append::rolling_file::policy::compound::CompoundPolicy;
use log4rs::append::rolling_file::policy::compound::{roll::fixed_window::FixedWindowRoller, trigger::size::SizeTrigger};
use log4rs::append::rolling_file::RollingFileAppender;
use log4rs::config::Logger;
use log4rs::{
    config::{Appender, Root},
    encode::{Encode, Write},
    Config,
};
use serde::ser::{self, Serialize, SerializeMap};
use std::{env, fs};
use std::{fmt, option, thread};
use time::OffsetDateTime;
use uuid::Uuid;

/// An `Encode`r which writes a JSON object.
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug, Default)]
pub struct JsonEncoderAdvanced(());

impl JsonEncoderAdvanced {
    /// Returns a new `JsonEncoder` with a default configuration.
    pub fn new() -> Self {
        Self::default()
    }
}

#[allow(dead_code)]
#[cfg(not(windows))]
const NEWLINE: &str = "\n";

const MARKER: &str = "marker";

const LOGS_DIR: &str = "logs";

impl JsonEncoderAdvanced {
    fn encode_inner(&self, w: &mut dyn Write, time: DateTime<Local>, record: &Record) -> anyhow::Result<()> {
        let thread = thread::current();
        let marker = record.key_values().get(MARKER.into()).map(|it| it.to_string());
        let message = Message {
            id: &Uuid::new_v4().to_string(),
            time: time.format_with_items(Some(Item::Fixed(Fixed::RFC3339)).into_iter()),
            message: record.args(),
            level: record.level(),
            module_path: record.module_path(),
            source: record.file(),
            line: record.line(),
            marker,
            target: record.target(),
            thread: thread.name(),
            thread_id: thread_id::get(),
            mdc: Mdc,
        };
        message.serialize(&mut serde_json::Serializer::new(&mut *w))?;
        w.write_all(NEWLINE.as_bytes())?;
        Ok(())
    }
}

impl Encode for JsonEncoderAdvanced {
    fn encode(&self, w: &mut dyn Write, record: &Record) -> anyhow::Result<()> {
        self.encode_inner(w, Local::now(), record)
    }
}

#[derive(serde::Serialize)]
struct Message<'a> {
    #[serde(rename = "guid")]
    id: &'a str,
    #[serde(serialize_with = "ser_display", rename = "@t")]
    time: DelayedFormat<option::IntoIter<Item<'a>>>,
    level: Level,
    #[serde(skip_serializing_if = "Option::is_none")]
    marker: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    line: Option<u32>,
    target: &'a str,
    thread: Option<&'a str>,
    thread_id: usize,
    mdc: Mdc,
    #[serde(serialize_with = "ser_display", rename = "@m")]
    message: &'a fmt::Arguments<'a>,
    #[serde(skip_serializing_if = "Option::is_none")]
    module_path: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none", rename = "src")]
    source: Option<&'a str>,
}

fn ser_display<T, S>(v: &T, s: S) -> Result<S::Ok, S::Error>
where
    T: fmt::Display,
    S: ser::Serializer,
{
    s.collect_str(v)
}

struct Mdc;

impl ser::Serialize for Mdc {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: ser::Serializer,
    {
        let mut map = serializer.serialize_map(None)?;

        let mut err = Ok(());
        log_mdc::iter(|k, v| {
            if let Ok(()) = err {
                err = map.serialize_key(k).and_then(|()| map.serialize_value(v));
            }
        });
        err?;

        map.end()
    }
}

pub fn init_logs() {
    if true {
        init_log4rs();
    } else {
        init_fern();
    }
}

fn init_fern() {
    fs::create_dir_all(LOGS_DIR).expect("xboCjC");
    fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "[{} {} {}] {:?} {}",
                OffsetDateTime::now_utc(),
                record.level(),
                record.target(),
                record.key_values().get("marker".into()),
                message
            ))
        })
        .level(log::LevelFilter::Debug)
        .level_for("hyper", log::LevelFilter::Info)
        .chain(std::io::stdout())
        .chain(fern::log_file(format!("{}/logs.ndjson", LOGS_DIR)).unwrap())
        .apply()
        .unwrap();
}

fn init_log4rs() {
    fs::create_dir_all(LOGS_DIR).expect("Uvjjq0");
    let trigger_size: u64 = Byte::parse_str("200 MB", true).unwrap().as_u64();
    let trigger = Box::new(SizeTrigger::new(trigger_size));
    let roller_pattern = &format!("{}/bomapp_server_{{}}.gz", &LOGS_DIR);
    println!("{}", roller_pattern);
    let roller_count = 5;
    let roller_base = 1;
    let roller = Box::new(FixedWindowRoller::builder().base(roller_base).build(roller_pattern, roller_count).unwrap());

    let compound_policy = Box::new(CompoundPolicy::new(trigger, roller));

    let step_ap = RollingFileAppender::builder()
        .encoder(Box::new(JsonEncoderAdvanced::new()))
        .build(format!("{}/bomapp_server.ndjson", &LOGS_DIR), compound_policy)
        .unwrap();

    let config = Config::builder()
        .appender(Appender::builder().build("step_ap", Box::new(step_ap)))
        .logger(Logger::builder().appender("step_ap").build("step", LevelFilter::Debug))
        .build(Root::builder().appender("step_ap").build(LevelFilter::Debug))
        .unwrap();

    // You can use handle to change logger config at runtime
    let _handle = log4rs::init_config(config).unwrap();
}
