use base64::engine::general_purpose;
use base64::Engine;
use chrono::{DateTime, Utc};
use gitlab_config::GitlabConfig;
use serde_json::Value;
use std::{env, fs};
use std::fs::OpenOptions;
use std::io::{Read, Seek, SeekFrom, Write};
use std::path::Path;
use std::process::exit;
use std::str;
use std::time::{Duration, SystemTime};
use std::process;
mod gitlab_config;
use current_platform::{COMPILED_ON, CURRENT_PLATFORM};
mod app_state;
mod websocket;
use actix_web::middleware::Logger as WsLogger;
mod logs;
use actix::{Actor, Addr};
use actix_web::{
    get,
    web::{self, Data},
    App, HttpRequest, HttpResponse, HttpServer, Responder,
};
use fs::File;

const DEBUG: bool = false;

const CONFIG_FILE: &str = if DEBUG {
    "./target/remote_scripts_executor.json"
} else {
    "/etc/remote_scripts_executor/remote_scripts_executor.json"
};
const PID_FILE: &str = if DEBUG {
    "./target/remote_scripts_executor.pid"
} else {
    "/var/run/remote_scripts_executor.pid"
};
const LOG_FILE: &str = if DEBUG {
    "./target/remote_scripts_executor.log"
} else {
    "/var/log/remote_scripts_executor/remote_scripts_executor.log"
};

fn main() {
    ctrlc::set_handler(move || {
        println!("{} | {} | Ctrl+C received, exiting", time_now(), "YcrMH1");
        std::process::exit(0);
    })
    .expect(&format!("{} | {} | Error setting Ctrl-C handler", time_now(), "vvy9aj"));

    println!("{} | {} | CURRENT_PLATFORM = {}", time_now(), "Yj0edu", CURRENT_PLATFORM);
    println!("{} | {} | COMPILED_ON = {}", time_now(), "Nfx6xo", COMPILED_ON);

    let config_exists = Path::new(CONFIG_FILE).exists();
    if !config_exists {
        eprintln!("{} | {} | File {} not found", time_now(), "zd11by", CONFIG_FILE);
        exit(1);
    }
    let config_string = fs::read_to_string(CONFIG_FILE).unwrap();
    let config: Value = serde_json::from_str(&*config_string).unwrap();
    let port = config.get("port").unwrap().as_u64().unwrap();

    // // LOGGER:
    // logs::init_logs();
    // info!(marker = "AIBHpS"; "LOGGER IS OK");

    let runtime = actix_rt::Runtime::new().unwrap();
    runtime.block_on(async move {
        let addr_ws_connections_storage = websocket::websocket_connection::WebsocketConnections::new().start();
        let addr_ws_clone = addr_ws_connections_storage.clone();
        let loop_handle = tokio::spawn(loop_over(config, addr_ws_clone.clone()));
        // let serve_handle = tokio::spawn(serve(addr_ws_connections_storage.clone()));
        let serve_handle = serve(port, &config_string, addr_ws_connections_storage.clone());
        let (first, second) = tokio::join!(loop_handle, serve_handle);
    });
}

async fn loop_over(config: Value, addr_ws_connections_storage: Addr<websocket::websocket_connection::WebsocketConnections>) {
    println!("{} | {} | Looping over started", time_now(), "sZypBt");

    let no_proxy = config.get("no_proxy").unwrap().as_bool().unwrap();
    let period_sec = config.get("period_sec").unwrap().as_u64().unwrap();
    let pid_exists = Path::new(PID_FILE).exists();
    let log_exists = Path::new(LOG_FILE).exists();

    let mut pid_file = OpenOptions::new()
        .append(false)
        .write(true)
        .read(true)
        .truncate(true)
        .create_new(!pid_exists)
        .open(PID_FILE)
        .unwrap();

    let mut log_file = OpenOptions::new()
        .append(true)
        .write(true)
        .read(false)
        .truncate(false)
        .create_new(!log_exists)
        .open(LOG_FILE)
        .unwrap();

    let pid = process::id();
    pid_file.write_all(format!("{}\n", pid).as_ref()).unwrap();
    pid_file.flush().unwrap();

    // Create reqwest client with async support
    let http_client = reqwest::Client::builder().danger_accept_invalid_certs(true).timeout(Duration::from_secs(4)).no_trust_dns();
    let http_client = if no_proxy { http_client.no_proxy() } else { http_client };
    let http_client = http_client.build().unwrap();

    let remote = config.get("remote").unwrap().as_str().unwrap();
    let mut i = 0;
    if remote.eq("gitlab") {
        let gitlab_config: GitlabConfig = serde_json::from_value(config).unwrap();
        tokio::time::sleep(Duration::from_secs(period_sec)).await;
        loop {
            i = i + 1;
            log_and_send(format!("{} | {} | Loop iteration №{}", time_now(), "NApv68", i), &log_file, &addr_ws_connections_storage);

            for gitlab in &gitlab_config.scripts {
                if gitlab.skip {
                    continue;
                }

                let script_exists = Path::new(&gitlab.script_file).exists();
                let mut script_file = OpenOptions::new()
                    .append(false)
                    .write(true)
                    .read(true)
                    .truncate(false)
                    .create_new(!script_exists)
                    .open(gitlab.script_file.clone())
                    .unwrap();

                let mut access_token: String = gitlab.access_token.clone();
                if access_token.starts_with("$") {
                    match env::var(access_token[1..].to_string()) {
                        Ok(it) => {
                            access_token = it;
                        },
                        Err(e) => {
                            log_and_send(
                                format!("{} | {} | Error: {}", time_now(), "cJ7wG3", e),
                                &log_file,
                                &addr_ws_connections_storage,
                            );
                        },
                    }
                }

                let response = http_client.get(gitlab.script_url.clone()).header("PRIVATE-TOKEN", access_token).send().await;
                match response {
                    Ok(it) => {
                        if 200 == it.status() {
                            let mut script = String::new();
                            script_file.seek(SeekFrom::Start(0)).unwrap();
                            script_file.read_to_string(&mut script).unwrap();

                            let body: Value = it.json().await.unwrap();
                            let content_base64_str = body.get("content").unwrap().as_str().unwrap();
                            let decoded = general_purpose::STANDARD.decode(content_base64_str).unwrap();
                            let content = String::from_utf8(decoded).unwrap();

                            if !script.eq(&content) {
                                script_file.seek(SeekFrom::Start(0)).unwrap();
                                script_file.set_len(0).unwrap();
                                script_file.flush().unwrap();
                                script_file.seek(SeekFrom::Start(0)).unwrap();
                                script_file.write_all(content.as_ref()).unwrap();
                                script_file.flush().unwrap();

                                log_and_send(
                                    format!("{} | {} | File {} has changed", time_now(), "sJeFwh", gitlab.script_file),
                                    &log_file,
                                    &addr_ws_connections_storage,
                                );

                                // Run script in a blocking task since run_script! is synchronous
                                let (code, output, error) = tokio::task::spawn_blocking(move || run_script::run_script!(content).unwrap()).await.unwrap();

                                log_and_send(format!("{} | {} | Exit code: {}", time_now(), "pXMgp9", code), &log_file, &addr_ws_connections_storage);
                                log_and_send(
                                    format!("{} | {} | Output: {}", time_now(), "vcpfc3", output.trim()),
                                    &log_file,
                                    &addr_ws_connections_storage,
                                );
                                log_and_send(format!("{} | {} | Error: {}", time_now(), "5KKyjt", error.trim()), &log_file, &addr_ws_connections_storage);
                            }
                        } else {
                            log_and_send(
                                format!("{} | {} | HTTP error, status code: {}", time_now(), "GscxgU", it.status()),
                                &log_file,
                                &addr_ws_connections_storage,
                            );
                        }
                    }
                    Err(error) => {
                        log_and_send(format!("{} | {} | HTTP error: {}", time_now(), "XNelpm", error), &log_file, &addr_ws_connections_storage);
                    }
                }
            }

            tokio::time::sleep(Duration::from_secs(period_sec)).await;
        }
    } else {
        eprintln!("{} | {} | Only gitlab remote is implemented for now on", time_now(), "6D5OZZ");
        exit(1);
    }
}

fn log_and_send(log: String, mut log_file: &File, addr_ws_connections_storage: &Addr<websocket::websocket_connection::WebsocketConnections>) {
    log_file.write_all(log.as_ref()).unwrap();
    log_file.write_all("\n".as_ref()).unwrap();
    log_file.flush().unwrap();
    addr_ws_connections_storage.do_send(websocket::websocket_connection::SendRaw { text: log });
}

async fn serve(port: u64, config_string: &str, addr_ws_connections_storage: Addr<websocket::websocket_connection::WebsocketConnections>) {
    let addrs = format!("0.0.0.0:{}", port);
    println!("{} | {} | Serving at http://{}/", time_now(), "3iKX9p", addrs);

    let config_string = config_string.to_string();
    let _ = HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(app_state::AppState {
                addr_ws_connections_storage: addr_ws_connections_storage.clone(),
                config: config_string.clone(),
            }))
            .service(root)
            .service(favicon)
            .service(get_data)
            .route("/api/ws", web::get().to(websocket::websocket_handler::ws))
            .wrap(WsLogger::default())
    })
    .workers(1)
    .bind(addrs)
    .unwrap()
    .run()
    .await;
}

#[get("/")]
async fn root(http_request: HttpRequest) -> impl Responder {
    return HttpResponse::Ok().content_type("text/html").body(include_str!("index.html"));
}

#[get("/favicon.ico")]
async fn favicon() -> impl Responder {
    return HttpResponse::Ok().content_type("image/vnd.microsoft.icon").body(include_bytes!("favicon.ico").to_vec());
}

#[get("/api/data")]
async fn get_data(http_request: HttpRequest) -> impl Responder {
    let app_state = http_request.app_data::<Data<app_state::AppState>>().unwrap();
    return HttpResponse::Ok().content_type("application/json").body(app_state.config.clone());
}

fn time_now() -> String {
    let now = SystemTime::now();
    let now: DateTime<Utc> = now.into();
    now.to_rfc3339()
}
