use actix::Addr;

use crate::websocket::websocket_connection::WebsocketConnections;
use serde_json::Value;

pub struct AppState {
    pub addr_ws_connections_storage: Addr<WebsocketConnections>,
    pub config: String,
}
