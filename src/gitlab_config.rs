use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct GitlabConfig {
    pub scripts: Vec<GitlabScript>,
}

#[derive(Serialize, Deserialize)]
pub struct GitlabScript {
    pub skip: bool,
    pub script_file: String,
    pub access_token: String,
    pub script_url: String,
}
